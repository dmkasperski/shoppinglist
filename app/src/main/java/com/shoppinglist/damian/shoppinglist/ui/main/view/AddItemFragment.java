package com.shoppinglist.damian.shoppinglist.ui.main.view;

import android.app.DialogFragment;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import com.shoppinglist.damian.shoppinglist.R;
import com.shoppinglist.damian.shoppinglist.ui.main.presenter.MainPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by damian on 25.11.17.
 */

public class AddItemFragment  extends DialogFragment {

    MainPresenter presenter;
    @BindView(R.id.edittext_item)
    EditText item;
    @BindView(R.id.edittext_amount)
    EditText amount;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        presenter = new MainPresenter(getContext());

        View view = View.inflate(getContext(), R.layout.add_item_fragment, null);
        ButterKnife.bind(this, view);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.button_add_item)
    void addItem() {
        presenter.addItem(item.getText().toString().trim(),amount.getText().toString().trim(), String.valueOf(presenter.getIndexOfOpenShoppingList()));
        }

}

