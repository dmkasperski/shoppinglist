package com.shoppinglist.damian.shoppinglist.ui.archive;

import android.database.Cursor;

import com.shoppinglist.damian.shoppinglist.model.Item;
import com.shoppinglist.damian.shoppinglist.model.ShoppingList;
import com.shoppinglist.damian.shoppinglist.ui.archive.view.ArchiveActivity;

import java.util.ArrayList;

/**
 * Created by damian on 26.11.17.
 */

public interface ArchiveInterface {
    interface View{

        ArchiveActivity getContext();
        void closeApp();

    }
    interface Presenter{
        ArrayList<ShoppingList> getListOfArchivedShoppingLists();
        void signout();
        ArrayList<Item> getListOfItemsOnArchivedShoppingList(int index);
         }
}
