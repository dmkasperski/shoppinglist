package com.shoppinglist.damian.shoppinglist.ui.archive.view;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.shoppinglist.damian.shoppinglist.R;
import com.shoppinglist.damian.shoppinglist.model.Item;
import com.shoppinglist.damian.shoppinglist.ui.archive.adapter.RecyclerAdapterArchive;
import com.shoppinglist.damian.shoppinglist.ui.archive.adapter.RecyclerAdapterItems;
import com.shoppinglist.damian.shoppinglist.ui.archive.presenter.ArchivePresenter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by damian on 26.11.17.
 */

@SuppressLint("ValidFragment")
public class ArchivedItemsListFragment extends DialogFragment implements RecyclerAdapterArchive.ListItemClickListener {

    ArchivePresenter presenter;
    @BindView(R.id.recyclerview_archive_list)
    RecyclerView recyclerView;

    ArrayList<Item> items = new ArrayList<>();
    @RequiresApi(api = Build.VERSION_CODES.M)
    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        presenter = new ArchivePresenter(getContext());

        View view = View.inflate(getContext(), R.layout.archive_items_list, null);
        ButterKnife.bind(this, view);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        RecyclerAdapterItems mAdapter = new RecyclerAdapterItems(items);
        recyclerView.setAdapter(mAdapter);
        return view;
    }
    public ArchivedItemsListFragment (ArrayList<Item> items){
        this.items=items;

    }

    @Override
    public void onListItemClick(int clickedItemIndex) {

    }
}

