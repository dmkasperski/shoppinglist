package com.shoppinglist.damian.shoppinglist.di.archive;

import android.content.Context;

import com.shoppinglist.damian.shoppinglist.ui.archive.ArchiveInterface;
import com.shoppinglist.damian.shoppinglist.ui.archive.presenter.ArchivePresenter;
import com.shoppinglist.damian.shoppinglist.ui.archive.view.ArchiveActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by damian on 26.11.17.
 */

@Module
public class ArchiveModule {
    private ArchiveActivity mView;
    private Context context;

    public ArchiveModule(ArchiveActivity view){
        this.mView=view;
        this.context=view.getContext();
    }
    @Provides
    ArchiveInterface.View provideArchiveActivity(){
        return mView;
    }

    @Provides
    ArchivePresenter provideArchivePresenter(){
        return new ArchivePresenter(context);
    }
}