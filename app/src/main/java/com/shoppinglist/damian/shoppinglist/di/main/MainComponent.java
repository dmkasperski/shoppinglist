package com.shoppinglist.damian.shoppinglist.di.main;

import com.shoppinglist.damian.shoppinglist.di.app.AppComponent;
import com.shoppinglist.damian.shoppinglist.ui.main.presenter.MainPresenter;
import com.shoppinglist.damian.shoppinglist.ui.main.view.MainActivity;

import dagger.Component;

/**
 * Created by damian on 21.11.17.
 */
@Component(
        dependencies = {AppComponent.class},
        modules = {MainModule.class}

)
public interface MainComponent {
    //activity
    void inject(MainActivity view);

    //presenters
    void inject(MainPresenter presenter);
}
