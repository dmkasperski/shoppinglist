package com.shoppinglist.damian.shoppinglist.di.archive;

import com.shoppinglist.damian.shoppinglist.di.app.AppComponent;
import com.shoppinglist.damian.shoppinglist.ui.archive.presenter.ArchivePresenter;
import com.shoppinglist.damian.shoppinglist.ui.archive.view.ArchiveActivity;

import dagger.Component;

/**
 * Created by damian on 26.11.17.
 */

@Component(
        dependencies = {AppComponent.class},
        modules = {ArchiveModule.class}

)
public interface ArchiveComponent {
    //activity
    void inject(ArchiveActivity view);

    //presenters
    void inject(ArchivePresenter presenter);
}
