package com.shoppinglist.damian.shoppinglist.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


/**
 * Created by damian on 21.11.17.
 */

public class ShoppingListDBHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ShoppingList.db";

    private static final String SQL_CREATE_SHOPPING_LISTS =
            "CREATE TABLE " + ShoppingListContract.ShoppingList.TABLE_NAME + " (" +
                    ShoppingListContract.ShoppingList._ID + " INTEGER PRIMARY KEY," +
                    ShoppingListContract.ShoppingList.COLUMN_SHOPPING_LIST_NAME + " TEXT," +
                    ShoppingListContract.ShoppingList.COLUMN_TIMESTAMP + " TEXT," +
                    ShoppingListContract.ShoppingList.COLUMN_SHOPPING_LIST_STATE + " TEXT" +
            ")";

    private static final String SQL_DELETE_SHOPPING_LISTS =
            "DROP TABLE IF EXISTS " + ShoppingListContract.ShoppingList.TABLE_NAME;

    private static final String SQL_CREATE_SHOPPING_LISTS_DETAILS =
            "CREATE TABLE " + ShoppingListContract.ShoppingListDetails.TABLE_NAME + " (" +
                    ShoppingListContract.ShoppingListDetails._ID + " INTEGER PRIMARY KEY," +
                    ShoppingListContract.ShoppingListDetails.COLUMN_ITEM + " TEXT," +
                    ShoppingListContract.ShoppingListDetails.COLUMN_AMOUNT + " TEXT,"+
                    ShoppingListContract.ShoppingListDetails.COLUMN_SHOPPING_LIST_ID + " TEXT)";

    private static final String SQL_DELETE_SHOPPING_LISTS_DETAILS =
            "DROP TABLE IF EXISTS " + ShoppingListContract.ShoppingList.TABLE_NAME;


    public ShoppingListDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_SHOPPING_LISTS);
        Log.d("sql",SQL_CREATE_SHOPPING_LISTS_DETAILS);
        db.execSQL(SQL_CREATE_SHOPPING_LISTS_DETAILS);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_SHOPPING_LISTS);
        db.execSQL(SQL_DELETE_SHOPPING_LISTS_DETAILS);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
    public void add(String shoppingListName){
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ShoppingListContract.ShoppingList.COLUMN_SHOPPING_LIST_NAME,shoppingListName);
        values.put(ShoppingListContract.ShoppingList.COLUMN_TIMESTAMP,ts);
        values.put(ShoppingListContract.ShoppingList.COLUMN_SHOPPING_LIST_STATE,"1");
        db.insert(ShoppingListContract.ShoppingList.TABLE_NAME,null,values);


    }
    public Cursor query(){
        SQLiteDatabase db = this.getReadableDatabase();

        return db.query(ShoppingListContract.ShoppingList.TABLE_NAME,new String[] { ShoppingListContract.ShoppingList.COLUMN_SHOPPING_LIST_NAME, ShoppingListContract.ShoppingList.COLUMN_TIMESTAMP, ShoppingListContract.ShoppingList.COLUMN_SHOPPING_LIST_STATE, ShoppingListContract.ShoppingList._ID}, null, null,
                null, null, null, null);
    }
    public void changeState(){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ShoppingListContract.ShoppingList.COLUMN_SHOPPING_LIST_STATE,"0");
        Log.d("values", String.valueOf(values));
        db.update(ShoppingListContract.ShoppingList.TABLE_NAME,values,"",null);
    }
    public void addItem(String item, String amount, String index){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ShoppingListContract.ShoppingListDetails.COLUMN_ITEM,item);
        values.put(ShoppingListContract.ShoppingListDetails.COLUMN_AMOUNT,amount);
        values.put(ShoppingListContract.ShoppingListDetails.COLUMN_SHOPPING_LIST_ID,index);
        db.insert(ShoppingListContract.ShoppingListDetails.TABLE_NAME,null,values);
}
    public Cursor queryItems(int index){
        SQLiteDatabase db = this.getReadableDatabase();
        return db.query(ShoppingListContract.ShoppingListDetails.TABLE_NAME,new String[] {ShoppingListContract.ShoppingListDetails.COLUMN_ITEM,ShoppingListContract.ShoppingListDetails.COLUMN_AMOUNT,ShoppingListContract.ShoppingListDetails._ID}, ShoppingListContract.ShoppingListDetails.COLUMN_SHOPPING_LIST_ID+" = "+index, null,
                null, null, null, null);
    }
    public void deleteItem(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(ShoppingListContract.ShoppingListDetails.TABLE_NAME,"_id = "+id,null);
    }

}