package com.shoppinglist.damian.shoppinglist;

import android.app.Application;
import android.content.Context;



import com.shoppinglist.damian.shoppinglist.di.app.AppComponent;
import com.shoppinglist.damian.shoppinglist.di.app.AppModule;
import com.shoppinglist.damian.shoppinglist.di.app.DaggerAppComponent;


/**
 * Created by damian on 21.11.17.
 */

public class App extends Application {
    private AppComponent component;

    public AppComponent getComponent() {
        return component;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        this.setAppComponent();

    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
    private void setAppComponent() {
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }
}
