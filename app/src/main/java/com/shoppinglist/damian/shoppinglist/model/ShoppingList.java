package com.shoppinglist.damian.shoppinglist.model;

/**
 * Created by damian on 26.11.17.
 */

public class ShoppingList {
    public String name;
    public String id;
    public ShoppingList(String name,String id){
    this.name = name;
    this.id = id;
    }
    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }
}
