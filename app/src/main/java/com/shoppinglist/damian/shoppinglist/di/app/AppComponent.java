package com.shoppinglist.damian.shoppinglist.di.app;

import android.content.Context;

import com.shoppinglist.damian.shoppinglist.App;


import dagger.Component;

/**
 * Created by damian on 21.11.17.
 */

@Component(
        modules = {AppModule.class}
)
public interface AppComponent {
    void inject (App app);
    Context context();
    App app();

}
