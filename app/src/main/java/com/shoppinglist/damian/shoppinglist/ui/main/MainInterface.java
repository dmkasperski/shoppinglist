package com.shoppinglist.damian.shoppinglist.ui.main;

import com.shoppinglist.damian.shoppinglist.model.Item;
import com.shoppinglist.damian.shoppinglist.ui.main.view.MainActivity;

import java.util.ArrayList;

/**
 * Created by damian on 21.11.17.
 */

public interface MainInterface {
    interface View{

        MainActivity getContext();
        void closeApp();
        void startMainActivity();
        void setOpenShoppingListName(String currentShoppingListName);

    }
    interface Presenter{
        void signout();
        void createNewShoppingList(String shoppingListName);
        void getOpenShoppingList();
        int getIndexOfOpenShoppingList();
        void finishShoppingList();
        void addItem(String item, String amount, String index);
        ArrayList<Item> getItemsForShoppingList(int index);
        void deleteItem(int id);
    }
}