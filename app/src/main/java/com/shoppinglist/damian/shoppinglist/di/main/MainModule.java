package com.shoppinglist.damian.shoppinglist.di.main;

import android.content.Context;
import com.shoppinglist.damian.shoppinglist.ui.main.MainInterface;
import com.shoppinglist.damian.shoppinglist.ui.main.presenter.MainPresenter;
import com.shoppinglist.damian.shoppinglist.ui.main.view.MainActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by damian on 21.11.17.
 */
@Module
public class MainModule {
    private MainActivity mView;
    private Context context;

    public MainModule(MainActivity view){
        this.mView=view;
        this.context=view.getContext();
    }
    @Provides
    MainInterface.View provideMainActivity(){
        return mView;
    }

    @Provides
    MainPresenter provideMainPresenter(){
        return new MainPresenter(context);
    }
}
