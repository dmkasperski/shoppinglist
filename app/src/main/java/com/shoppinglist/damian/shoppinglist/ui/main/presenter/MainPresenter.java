package com.shoppinglist.damian.shoppinglist.ui.main.presenter;

import android.content.Context;
import android.database.Cursor;

import com.shoppinglist.damian.shoppinglist.db.ShoppingListContract;
import com.shoppinglist.damian.shoppinglist.db.ShoppingListDBHelper;
import com.shoppinglist.damian.shoppinglist.model.Item;
import com.shoppinglist.damian.shoppinglist.ui.main.MainInterface;

import java.util.ArrayList;

import javax.inject.Inject;


/**
 * Created by damian on 21.11.17.
 */

public class MainPresenter implements MainInterface.Presenter {
    private final MainInterface.View mView;
    private final Context mContext;
    public ShoppingListDBHelper shoppingListDBHelper;
    @Inject
    public MainPresenter(Context context) {
        mView = (MainInterface.View) context;
        mContext = context;
        this.shoppingListDBHelper = new ShoppingListDBHelper(mContext);

    }


    @Override
    public void signout() {
        mView.closeApp();
    }

    @Override
    public void createNewShoppingList(String shoppingListName) {
        shoppingListDBHelper.add(shoppingListName);
        mView.startMainActivity();
    }

    @Override
    public void getOpenShoppingList() {
        Cursor cursor = shoppingListDBHelper.query();

        String currentShoppingListName=null;
        while(cursor.moveToNext()){
            currentShoppingListName =cursor.getString(cursor.getColumnIndex(ShoppingListContract.ShoppingList.COLUMN_SHOPPING_LIST_NAME));
        }
        mView.setOpenShoppingListName(currentShoppingListName);


    }

    @Override
    public int getIndexOfOpenShoppingList() {
        Cursor cursor = shoppingListDBHelper.query();
        int lastIndex = cursor.getCount() - 1;
        int state = 0;
        while (cursor.moveToNext()) {
            state = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ShoppingListContract.ShoppingList.COLUMN_SHOPPING_LIST_STATE)));
        } if (state > 0) {
            return lastIndex;
        }else{
            return -1;
        }
    }

    @Override
    public void finishShoppingList() {
        shoppingListDBHelper.changeState();
        mView.startMainActivity();
    }

    @Override
    public void addItem(String item, String amount, String index) {
            shoppingListDBHelper.addItem(item,amount,index);
            mView.startMainActivity();
    }

    @Override
    public ArrayList<Item> getItemsForShoppingList(int index) {

        Cursor cursor = shoppingListDBHelper.queryItems(index);

        ArrayList<Item> list = new ArrayList<>();
        while(cursor.moveToNext()){
            String name =cursor.getString(cursor.getColumnIndex(ShoppingListContract.ShoppingListDetails.COLUMN_ITEM));
            String amount =cursor.getString(cursor.getColumnIndex(ShoppingListContract.ShoppingListDetails.COLUMN_AMOUNT));
            String id =cursor.getString(cursor.getColumnIndex(ShoppingListContract.ShoppingListDetails._ID));

            Item item = new Item(name,amount,id);
            list.add(item);
        }

        return list;
    }

    @Override
    public void deleteItem(int id) {
        shoppingListDBHelper.deleteItem(id);
        mView.startMainActivity();
    }
}
