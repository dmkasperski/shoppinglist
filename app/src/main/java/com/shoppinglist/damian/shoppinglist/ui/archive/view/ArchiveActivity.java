package com.shoppinglist.damian.shoppinglist.ui.archive.view;

import android.app.DialogFragment;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.shoppinglist.damian.shoppinglist.App;
import com.shoppinglist.damian.shoppinglist.R;
import com.shoppinglist.damian.shoppinglist.di.archive.ArchiveComponent;
import com.shoppinglist.damian.shoppinglist.di.archive.ArchiveModule;
import com.shoppinglist.damian.shoppinglist.di.archive.DaggerArchiveComponent;
import com.shoppinglist.damian.shoppinglist.model.Item;
import com.shoppinglist.damian.shoppinglist.model.ShoppingList;
import com.shoppinglist.damian.shoppinglist.ui.archive.ArchiveInterface;
import com.shoppinglist.damian.shoppinglist.ui.archive.adapter.RecyclerAdapterArchive;
import com.shoppinglist.damian.shoppinglist.ui.archive.presenter.ArchivePresenter;
import com.shoppinglist.damian.shoppinglist.ui.main.view.AddShoppingListFragment;
import com.shoppinglist.damian.shoppinglist.ui.main.view.FinishOpenShoppingListFragment;
import com.shoppinglist.damian.shoppinglist.ui.main.view.MainActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArchiveActivity extends AppCompatActivity implements ArchiveInterface.View, RecyclerAdapterArchive.ListItemClickListener{
    @Inject
    ArchivePresenter presenter;
    ArchiveComponent component;
    ArrayList<ShoppingList> list = new ArrayList<>();
    ArrayList<Item> listOfItemsOnChoosenShoppingList = new ArrayList<>();
@BindView(R.id.recycler_view_archive)
    RecyclerView rc_archive;
     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_archive);
         ButterKnife.bind(this);
        setComponent();
        list = presenter.getListOfArchivedShoppingLists();
         LinearLayoutManager layoutManager = new LinearLayoutManager(this);
         rc_archive.setLayoutManager(layoutManager);
         RecyclerAdapterArchive mAdapter = new RecyclerAdapterArchive(list,this);
         rc_archive.setAdapter(mAdapter);
     }
    private void setComponent() {
        if (component == null) {
            component = DaggerArchiveComponent.builder()
                    .appComponent(App.get(this).getComponent())
                    .archiveModule(new ArchiveModule(this))
                    .build();
            component.inject(this);
        }

    }

    @Override
    public ArchiveActivity getContext() {
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sign_out:
                presenter.signout();
                return true;
            case R.id.new_shopping_list:
                DialogFragment FragmentAddShoppingList = new AddShoppingListFragment();
                FragmentAddShoppingList.show(getFragmentManager(), "dialog");

                return true;
            case R.id.action_finish_shopping_list:
                DialogFragment FragmentFinishShoppingList = new FinishOpenShoppingListFragment();
                FragmentFinishShoppingList.show(getFragmentManager(), "dialog1");
                return true;
            case R.id.action_main:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        menu.findItem(R.id.new_shopping_list).setEnabled(false);
        menu.findItem(R.id.action_finish_shopping_list).setEnabled(false);
        menu.findItem(R.id.new_shopping_list).setVisible(false);
        menu.findItem(R.id.action_finish_shopping_list).setVisible(false);


        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
        }

    @Override
    public void closeApp() {
        finish();
    }

    @Override
    public void onListItemClick(int clickedItemIndex) {
    ArrayList<Item> items = new ArrayList<>();
    items = presenter.getListOfItemsOnArchivedShoppingList(clickedItemIndex);
    listOfItemsOnChoosenShoppingList = items;

        DialogFragment FragmentArchivedItemsList = new ArchivedItemsListFragment(items);
        FragmentArchivedItemsList.show(getFragmentManager(), "dialog4");
    }

}
