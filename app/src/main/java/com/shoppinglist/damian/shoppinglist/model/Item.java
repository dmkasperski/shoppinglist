package com.shoppinglist.damian.shoppinglist.model;

/**
 * Created by damian on 26.11.17.
 */

public class Item {
    public String name;
    public String amount;
    public String id;
    public Item(String name, String amount, String id){
        this.name = name;
        this.amount = amount;
        this.id = id;
    }
    public Item(String name){
        this.name = name;
        this.amount = "0";
    }

    public String getName() {
        return name;
    }

    public String getAmount() {
        return amount;
    }

    public String getId() {
        return id;
    }
}
