package com.shoppinglist.damian.shoppinglist.ui.main.view;

import android.annotation.SuppressLint;

import android.app.AlertDialog;
import android.app.DialogFragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import android.widget.TextView;
import android.widget.Toast;

import com.shoppinglist.damian.shoppinglist.App;
import com.shoppinglist.damian.shoppinglist.R;
import com.shoppinglist.damian.shoppinglist.db.ShoppingListDBHelper;
import com.shoppinglist.damian.shoppinglist.di.main.DaggerMainComponent;
import com.shoppinglist.damian.shoppinglist.di.main.MainComponent;
import com.shoppinglist.damian.shoppinglist.di.main.MainModule;
import com.shoppinglist.damian.shoppinglist.model.Item;
import com.shoppinglist.damian.shoppinglist.ui.archive.view.ArchiveActivity;
import com.shoppinglist.damian.shoppinglist.ui.main.MainInterface;
import com.shoppinglist.damian.shoppinglist.ui.main.adapter.RecyclerAdapter;
import com.shoppinglist.damian.shoppinglist.ui.main.presenter.MainPresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends AppCompatActivity implements MainInterface.View, RecyclerAdapter.ListItemClickListener {
    @Inject
    MainPresenter presenter;
    MainComponent component;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;


    @BindView(R.id.floatingActionButton)
    FloatingActionButton fab;
    @BindView(R.id.textView_no_shopping_list)
    TextView textview_no_shopping_list;

    int currentShoppingListIndex;
    ArrayList<Item> list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ShoppingListDBHelper mDbHelper = new ShoppingListDBHelper(getContext());

        setComponent();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);



        currentShoppingListIndex = presenter.getIndexOfOpenShoppingList();
        if (currentShoppingListIndex > -1) {
            presenter.getOpenShoppingList();

            Cursor cursorForItems = mDbHelper.queryItems(currentShoppingListIndex);
            DatabaseUtils.dumpCursor(cursorForItems);
        list=presenter.getItemsForShoppingList(currentShoppingListIndex);
        }



        RecyclerAdapter mAdapter = new RecyclerAdapter(list, this);
        recyclerView.setAdapter(mAdapter);


    }

    private void setComponent() {
        if (component == null) {
            component = DaggerMainComponent.builder()
                    .appComponent(App.get(this).getComponent())
                    .mainModule(new MainModule(this))
                    .build();
            component.inject(this);
        }

    }

    @Override
    public MainActivity getContext() {
        return this;
    }

    @Override
    public void closeApp() {
        finish();
    }

    @Override
    public void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void setOpenShoppingListName(String currentShoppingListName) {


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sign_out:
                presenter.signout();
                return true;
            case R.id.new_shopping_list:
                DialogFragment FragmentAddShoppingList = new AddShoppingListFragment();
                FragmentAddShoppingList.show(getFragmentManager(), "dialog");

                return true;
            case R.id.action_finish_shopping_list:
                DialogFragment FragmentFinishShoppingList = new FinishOpenShoppingListFragment();
                FragmentFinishShoppingList.show(getFragmentManager(), "dialog1");
                return true;
            case R.id.action_history:
              Intent intent = new Intent(this, ArchiveActivity.class);
              startActivity(intent);
              finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        currentShoppingListIndex = presenter.getIndexOfOpenShoppingList();
        if (currentShoppingListIndex < 0) {
            menu.findItem(R.id.new_shopping_list).setEnabled(true);
            menu.findItem(R.id.new_shopping_list).setVisible(true);
            menu.findItem(R.id.action_finish_shopping_list).setVisible(false);
            menu.findItem(R.id.action_finish_shopping_list).setEnabled(false);
        } else {
            menu.findItem(R.id.new_shopping_list).setVisible(false);
            menu.findItem(R.id.action_finish_shopping_list).setVisible(true);
            menu.findItem(R.id.new_shopping_list).setEnabled(false);
            menu.findItem(R.id.action_finish_shopping_list).setEnabled(true);
            fab.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
            textview_no_shopping_list.setVisibility(View.GONE);

        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onListItemClick(final int clickedItemIndex) {
        Log.d("el", String.valueOf(clickedItemIndex));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to delete this item?").setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String id = list.get(clickedItemIndex).getId();
                presenter.deleteItem(Integer.parseInt(id));

            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }
    @OnClick(R.id.floatingActionButton)
    void addItem(){
        DialogFragment FragmentAddItem = new AddItemFragment();
        FragmentAddItem.show(getFragmentManager(), "dialog2");
    }
}
