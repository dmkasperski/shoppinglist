package com.shoppinglist.damian.shoppinglist.ui.archive.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shoppinglist.damian.shoppinglist.R;
import com.shoppinglist.damian.shoppinglist.model.ShoppingList;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by damian on 22.11.17.
 */

public class RecyclerAdapterArchive extends RecyclerView.Adapter<RecyclerAdapterArchive.MyViewHolder> {
    private static final String TAG = RecyclerAdapterArchive.class.getSimpleName();

    final private RecyclerAdapterArchive.ListItemClickListener mOnClickListener;
    ArrayList<ShoppingList> mList;

    public interface ListItemClickListener {
        void onListItemClick(int clickedItemIndex);
    }

    public RecyclerAdapterArchive(ArrayList<ShoppingList> list, ListItemClickListener listener) {
        this.mList = list;
        Collections.reverse(mList);
        mOnClickListener = listener;

    }


    @Override
    public RecyclerAdapterArchive.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.archive_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;
        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RecyclerAdapterArchive.MyViewHolder holder, int position) {
       holder.archive_item_name.setText(mList.get(position).getName());
        holder.bind(position);

    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    @Override
    public int getItemCount() {
        return this.mList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        @BindView(R.id.textview_archive_name)
        TextView archive_item_name;
        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        void bind(int product) {
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(clickedPosition);

        }
    }

}
