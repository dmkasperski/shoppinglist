package com.shoppinglist.damian.shoppinglist.db;

import android.provider.BaseColumns;

/**
 * Created by damian on 21.11.17.
 */

public final class ShoppingListContract {
    private ShoppingListContract() {
    }

    public static class ShoppingList implements BaseColumns {
        public static final String TABLE_NAME = "shoppingList";
        public static final String COLUMN_TIMESTAMP = "timestamp";
        public static final String COLUMN_SHOPPING_LIST_NAME = "shoppingListName";
        public static final String COLUMN_SHOPPING_LIST_STATE = "shoppingListState";

    }

    public static class ShoppingListDetails implements BaseColumns {
        public static final String TABLE_NAME = "shoppingListDetails";
        public static final String COLUMN_ITEM = "item";
        public static final String COLUMN_AMOUNT = "amount";
        public static final String COLUMN_SHOPPING_LIST_ID = "shoppingListId";
    }
}
