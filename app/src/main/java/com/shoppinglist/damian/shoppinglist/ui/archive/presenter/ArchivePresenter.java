package com.shoppinglist.damian.shoppinglist.ui.archive.presenter;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import com.shoppinglist.damian.shoppinglist.db.ShoppingListContract;
import com.shoppinglist.damian.shoppinglist.db.ShoppingListDBHelper;
import com.shoppinglist.damian.shoppinglist.model.Item;
import com.shoppinglist.damian.shoppinglist.model.ShoppingList;
import com.shoppinglist.damian.shoppinglist.ui.archive.ArchiveInterface;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by damian on 26.11.17.
 */

public class ArchivePresenter implements ArchiveInterface.Presenter {
    private final ArchiveInterface.View mView;
    private final Context mContext;
    public ShoppingListDBHelper shoppingListDBHelper;

    @Inject
    public ArchivePresenter(Context context) {
        mView = (ArchiveInterface.View) context;
        mContext = context;
        this.shoppingListDBHelper = new ShoppingListDBHelper(mContext);
    }

    @Override
    public ArrayList<ShoppingList> getListOfArchivedShoppingLists() {
        Cursor cursor = shoppingListDBHelper.query();
        DatabaseUtils.dumpCursor(cursor);
        ArrayList<ShoppingList> list = new ArrayList<>();
        while(cursor.moveToNext()){
            String state = cursor.getString(cursor.getColumnIndex(ShoppingListContract.ShoppingList.COLUMN_SHOPPING_LIST_STATE));
            if(Integer.parseInt(state)<1){
            String name =cursor.getString(cursor.getColumnIndex(ShoppingListContract.ShoppingList.COLUMN_SHOPPING_LIST_NAME));
            String id = cursor.getString(cursor.getColumnIndex(ShoppingListContract.ShoppingList._ID));
            ShoppingList shoppingList = new ShoppingList(name, id);
            list.add(shoppingList);
            }
        }
        return list;
    }

    @Override
    public void signout() {
        mView.closeApp();
    }

    @Override
    public ArrayList<Item> getListOfItemsOnArchivedShoppingList(int index) {
        ArrayList<Item> items = new ArrayList<>();
        Cursor cursor = shoppingListDBHelper.queryItems(index);
        DatabaseUtils.dumpCursor(cursor);
        while(cursor.moveToNext()){
            String name = cursor.getString(cursor.getColumnIndex(ShoppingListContract.ShoppingListDetails.COLUMN_ITEM));
            String amount = cursor.getString(cursor.getColumnIndex(ShoppingListContract.ShoppingListDetails.COLUMN_AMOUNT));
            String id = cursor.getString(cursor.getColumnIndex(ShoppingListContract.ShoppingListDetails._ID));
            Item item = new Item(name,amount,id);
            items.add(item);
        }
        return items;
    }

}
