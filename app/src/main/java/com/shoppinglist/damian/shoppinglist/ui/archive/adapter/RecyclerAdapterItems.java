package com.shoppinglist.damian.shoppinglist.ui.archive.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shoppinglist.damian.shoppinglist.R;
import com.shoppinglist.damian.shoppinglist.model.Item;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by damian on 26.11.17.
 */


public class RecyclerAdapterItems extends RecyclerView.Adapter<RecyclerAdapterItems.MyViewHolder> {
    private static final String TAG = RecyclerAdapterArchive.class.getSimpleName();

    ArrayList<Item> mList;



    public RecyclerAdapterItems(ArrayList<Item> list) {
        this.mList = list;


    }


    @Override
    public RecyclerAdapterItems.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.items_list_archive_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;
        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RecyclerAdapterItems.MyViewHolder holder, int position) {
        holder.item_archived_name.setText(mList.get(position).getName());
        holder.item_archived_amount.setText(mList.get(position).getAmount());
        holder.bind(position);

    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    @Override
    public int getItemCount() {
        return this.mList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_archived_amount)
        TextView item_archived_amount;
        @BindView(R.id.item_archived_name)
        TextView item_archived_name;
        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }

        void bind(int product) {
        }


    }

}

