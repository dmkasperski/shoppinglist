package com.shoppinglist.damian.shoppinglist.di.app;

import android.content.Context;

import com.shoppinglist.damian.shoppinglist.App;


import dagger.Module;
import dagger.Provides;

/**
 * Created by damian on 21.11.17.
 */
@Module
public class AppModule {
    private App app;

    public AppModule(App app) {
        this.app = app;
    }
    @Provides
    public App provideApplication() {
        return app;
    }

    @Provides
    public Context provideContext() {
        return app;
    }

}
