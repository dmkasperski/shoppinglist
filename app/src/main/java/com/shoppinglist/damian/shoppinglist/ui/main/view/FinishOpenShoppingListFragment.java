package com.shoppinglist.damian.shoppinglist.ui.main.view;

import android.app.DialogFragment;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.shoppinglist.damian.shoppinglist.R;
import com.shoppinglist.damian.shoppinglist.ui.main.presenter.MainPresenter;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by damian on 25.11.17.
 */

public class FinishOpenShoppingListFragment  extends DialogFragment{

        MainPresenter presenter;
        @RequiresApi(api = Build.VERSION_CODES.M)
        @NonNull
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            presenter = new MainPresenter(getContext());

            View view = View.inflate(getContext(), R.layout.finish_shoppinglist_fragment, null);
            ButterKnife.bind(this, view);
            getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
            getDialog().setCanceledOnTouchOutside(true);
            return view;
        }

        @RequiresApi(api = Build.VERSION_CODES.M)
        @OnClick(R.id.button_finish_shopping_list)
        void finishShoppingList() {
            presenter.finishShoppingList();
        }

    }

